// const webpackConfig = require('./config/webpack/test.js');

// // Compile only required files
// webpackConfig.entry = null;
// // webpackConfig.mode = 'development';
// webpackConfig.devtool = 'inline-source-map';
// // console.log(webpackConfig)


/**
 * To load fixtures use following path:
 * fetch('fixtures/test.json')
 */

const preprocessors = ['webpack', 'sourcemap'];
const path = require('path');

module.exports = function Configure(config) {
  config.set({
    basePath: 'tests',
    frameworks: ['mocha', 'chai'],
    plugins: [
      'karma-mocha',
      'karma-chai',
      'karma-coverage',
      'karma-webpack',
      'karma-sourcemap-loader',
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-safari-launcher',
      'karma-coverage-istanbul-reporter' /* optional */,
      'karma-spec-reporter', /* optional */
    ],
    files: [
      '*.js',
    ],

    exclude: [],
    webpack: {
      mode: 'development',
      module: {
        rules: [
          // instrument only testing sources with Istanbul
          {
            test: /\.js$/,
            use: { loader: 'istanbul-instrumenter-loader', options: { esModules: true } },
            include: path.resolve('src'),
          },
        ],
      },
    },
    preprocessors: {
      '*.js': preprocessors,
    },
    reporters: ['progress'/* , 'coverage-istanbul' */],
    coverageIstanbulReporter: {
      reports: ['html', 'text-summary'],
      fixWebpackSourcePaths: true,
    } /* optional */,
    // port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless', 'FirefoxHeadless'],
    singleRun: false,
  });
};
