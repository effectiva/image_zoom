import { ZOOM_CONSTANT, easeOutQuart } from '../src/util';


describe('Util', () => {
  it('ZOOM_CONSTANT', () => {
    assert.equal(ZOOM_CONSTANT, 15);
  });

  it('easeOutQuart', () => {
    assert.equal(easeOutQuart(1, 1, 1, 1), 2);
  });

});
