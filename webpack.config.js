const path = require('path');


module.exports = (env = 'development') => {
  console.log(env)
  const isProduction = env === 'production';

  const entries = {
    main: './src/index.js',
  };

  if (!isProduction) {
    entries.example = './example/index.js';
  }

  const outputPath = `${__dirname}/${isProduction ? 'lib' : 'dist'}`;

  return {


    mode: env,
    entry: entries,
    devtool: 'source-map',
    output: {
      path: outputPath,
      filename: '[name].js',
      library: 'ImageViewer',
      libraryTarget: 'umd',
    },
    devServer: {
      host: '0.0.0.0',
      contentBase: [
        path.join(__dirname, 'example'),
        path.join(__dirname, 'dist'),
      ],
    },
    module: {
      rules: [
      // {
      //   enforce: 'pre',
      //   test: /\.(js|jsx)$/,
      //   exclude: /node_modules/,
      //   use: 'eslint-loader',
      // },

        {
          test: /\.js$/,
          loader: 'babel-loader',
          options: {
            cacheDirectory: 'tmp/babel-loader-node-modules',
            cacheCompression: false,
            compact: false,
            sourceMaps: true,
          },
        },


        {
          test: /\.scss$/,
          use: [
            'style-loader',
            'css-loader',
            'sass-loader',
          ],
        },

        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: () => [require('postcss-import')(), require('postcss-cssnext')(), require('autoprefixer')(), require('cssnano')()],
              },
            },
          ],
        },
      ],
    },
  };
};
