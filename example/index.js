import '../src/ImageViewer.scss';
import FullScreenViewer from '../src/index';


document.querySelectorAll('.gallery-items').forEach((elem) => {
  elem.addEventListener('click', () => {
    const viewer = new FullScreenViewer();
    const highResolutionImage = elem.getAttribute('data-high-res-src');
    viewer.show(elem.src, highResolutionImage);
  });
});
